.global _my_function, _another_function, _function_three

.text

_my_function:
mov $4, %ebx
mov $2, %edi
imul %edi, %ebx
mov $9, %esi
not %esi
add %esi, %ebx
mov $1, %ecx
sub %ecx, %ebx
mov %ebx, %eax
ret


_another_function:
	pushq	%rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movl $10, -4(%rbp)
	decl -4(%rbp)
	movl -4(%rbp), %eax
	addq $16, %rsp
	popq %rbp
	retq

_function_three:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	movl	$5, -4(%rbp)
	movl	-4(%rbp), %ecx
	cmp	$8, %ecx
	setl	%al
	test	%al, %al
	jz	L201
	movl	$9, -4(%rbp)
	jmp	L202
L201:
	movl	$10, -4(%rbp)
L202:
	movl	-4(%rbp), %eax
	movq	%rbp, %rsp
	popq	%rbp
	ret
