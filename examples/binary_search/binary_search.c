
int test_function(int* array) {
    return *array;
}

int binary_search(int* array, int length, int key) {
    int begin = 0;
    int end = length;
    int half;

    while (begin < end) {
        half = (begin + end) / 2;
        return array[half];
    }

    return -1;
}

