
			pushq   %rbp
0000000100003ef1        movq    %rsp, %rbp
0000000100003ef4        movq    %rdi, -0x8(%rbp)
0000000100003ef8        movq    -0x8(%rbp), %rax
0000000100003efc        movl    (%rax), %eax
0000000100003efe        popq    %rbp
0000000100003eff        retq

.text
_test_function:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	mov	%rsi, -8(%rbp)  # setup arg 'array'
	movq	-8(%rbp), %rcx
	movl	(%rcx), %eax
	movq	%rbp, %rsp
	popq	%rbp
	ret
_binary_search:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	mov	%rsi, -8(%rbp)  # setup arg 'array'
	mov	%esi, -12(%rbp)  # setup arg 'length'
	mov	%edx, -16(%rbp)  # setup arg 'key'
	subq	$16, %rsp
	movl	$0, -20(%rbp)
	movl	-12(%rbp), %ecx
	movl	%ecx, -24(%rbp)
L101:
	movl	-24(%rbp), %ecx
	movl	-20(%rbp), %esi
	cmp	%ecx, %esi
	setl	%al
	test	%al, %al
	jz	L102
	movl	-24(%rbp), %esi
	movl	-20(%rbp), %eax
	add	%esi, %eax
	cdq
	mov	$2, %esi
	idivl	%esi
	mov	%eax, %ecx
	movl	%ecx, -28(%rbp)
	movq	-8(%rbp), %rsi
	movl	-28(%rbp), %ecx
	mov	(%rsi, %rcx, 4), %edi
	mov	%edi, %eax
	movq	%rbp, %rsp
	popq	%rbp
	ret
	jmp	L101
L102:
	movl	$1, %eax
	neg	%eax
	movq	%rbp, %rsp
	popq	%rbp
	ret
.global _test_function, _binary_search
