
#include <stdio.h>


int binary_search(int* array, int length, int key);
int test_function(int* array);


// This example program is not done yet and the asmgen part of the project
// needs additional functionality before binary seach will work.
int main() {
    int array[] = {1, 2, 5, 6, 7, 9};
    int length = sizeof(array) / sizeof(*array);

    printf("output: %i\n", test_function(array));
}
