
int clamp(int value, int min, int max) {
    int ret = value;

    if (value < min) {
        ret = min;
    } else if (value > max) {
        ret = max;
    }

    return ret;
}
