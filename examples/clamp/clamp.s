.text
_clamp:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	mov	%edi, -4(%rbp)  # setup arg 'value'
	mov	%esi, -8(%rbp)  # setup arg 'min'
	mov	%edx, -12(%rbp)  # setup arg 'max'
	movl	-4(%rbp), %ecx
	movl	%ecx, -16(%rbp)
	movl	-8(%rbp), %ecx
	movl	-4(%rbp), %esi
	cmp	%ecx, %esi
	setl	%al
	test	%al, %al
	jz	L101
	movl	-8(%rbp), %ecx
	movl	%ecx, -16(%rbp)
	jmp	L102
L101:
	movl	-12(%rbp), %ecx
	movl	-4(%rbp), %esi
	cmp	%ecx, %esi
	setg	%al
	test	%al, %al
	jz	L103
	movl	-12(%rbp), %ecx
	movl	%ecx, -16(%rbp)
L103:
L102:
	movl	-16(%rbp), %eax
	movq	%rbp, %rsp
	popq	%rbp
	ret
.global _clamp
