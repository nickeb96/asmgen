
#include <stdio.h>


int clamp(int value, int min, int max);


int main() {
    int values[] = {-3, 7, 10, 14, 19, 20, 25};
    int length = sizeof(values) / sizeof(*values);
    int min = 10;
    int max = 20;

    for (int i = 0; i < length; i++) {
        int x = clamp(values[i], min, max);
        printf("clamp(%i, %i, %i) is %i\n", values[i], min, max, x);
    }
}
