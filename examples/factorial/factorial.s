.global _factorial

.text

_factorial:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	mov	%edi, -4(%rbp)  # setup arg 'n'
	movl	$1, -8(%rbp)
L201:
	movl	-4(%rbp), %ecx
	cmp	$2, %ecx
	setge	%al
	test	%al, %al
	jz	L202
	movl	-4(%rbp), %edi
	movl	-8(%rbp), %esi
	imul	%edi, %esi
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	sub	$1, %esi
	movl	%esi, -4(%rbp)
	jmp	L201
L202:
	movl	-8(%rbp), %eax
	movq	%rbp, %rsp
	popq	%rbp
	ret

