
use astgen::expression::Expression;
use astgen::expression::operator::{PreOp, PostOp, BinOp};
use astgen::ctype::CType;
use astgen::ctype::cinteger::{CInteger, Signedness};

use crate::stack_manager::StackManager;


pub fn determine_output(expression: &Expression, stack: &StackManager) -> Result<CType, String> {
    match expression {
        Expression::IntegerLiteral(_) => Ok(CType::Integer(CInteger::Int(Signedness::Signed))),
        Expression::Identifier(ident) => {
            // TODO: make this work with globar vars/consts
            if let Some(stack_var) = stack.lookup(ident) {
                Ok(stack_var.ctype.clone())
            } else {
                Err(format!("Identifier {:?} does not exist", ident))
            }
        }
        Expression::PrefixOperator(PreOp::Promote, _) => unimplemented!(),
        // logical not, always returns int
        Expression::PrefixOperator(PreOp::Not, _) => Ok(CType::Integer(CInteger::Int(Signedness::Signed))),
        Expression::PrefixOperator(PreOp::Deref, child) => {
            let child_ctype = determine_output(child, stack)?;
            if let CType::Pointer(box pointee_ctype) = child_ctype {
                Ok(pointee_ctype.clone())
            } else {
                Err(format!("Dereference of non-pointer type: {:?}", child_ctype))
            }
        }
        Expression::PrefixOperator(PreOp::Addr, child) => {
            Ok(CType::Pointer(box determine_output(child, stack)?))
        }
        Expression::PrefixOperator(op, child) => {
            determine_output(child, stack)
        }
        Expression::PostfixOperator(PostOp::Inc | PostOp::Dec, child) => {
            determine_output(child, stack)
        }
        Expression::PostfixOperator(PostOp::FnCall, _) => unimplemented!(),
        Expression::PostfixOperator(PostOp::FieldAccess(field), base) => {
            if let CType::Struct(st) = determine_output(base, stack)? {
                if let Some((_, ctype)) = st.fields().iter().find(|(ident, _)| ident == field) {
                    Ok(ctype.clone())
                } else {
                    Err(format!("Struct {:?} has no field {:?}", st.name(), field))
                }
            } else {
                Err(format!("Field access on non-struct type"))
            }
        }
        Expression::PostfixOperator(PostOp::Subscript(_), base) => {
            match determine_output(base, stack)? {
                CType::Pointer(box pointee_ctype) => Ok(pointee_ctype.clone()),
                CType::Array(box element_ctype, _) => Ok(element_ctype.clone()),
                other => Err(format!("Subscript of type other than pointer or array: {:?}", other)),
            }
        }
        Expression::BinaryOperator(op, lhs, rhs) => {
            Ok(ctype_promotion(&determine_output(lhs, stack)?, &determine_output(rhs, stack)?))
        }
    }
}

fn ctype_promotion(lhs: &CType, rhs: &CType) -> CType {
    use CType::*;
    use CInteger::*;
    use Signedness::*;
    match (lhs, rhs) {
        // Binary operations on integers and pointers (which arrays can be implicitly
        // coerced into) always result in a pointer.
        (left @ (Pointer(_) | Array(_, _)), Integer(_)) => left.clone(),
        (Integer(_), right @ (Pointer(_) | Array(_, _))) => right.clone(),
        // If one operand is `int` and the other is lower ranked than
        // `int`, the resulting type is `int` carrying the signed/unsigned
        // qualifier from the `int` operand.
        (Integer(Char(_) | Short(_)), Integer(Int(sign))) => Integer(Int(*sign)),
        (Integer(Int(sign)), Integer(Char(_) | Short(_))) => Integer(Int(*sign)),
        // If both operands are lower rank than `int` and `unsigned int`,
        // the result is always a signed `int`, regardless of operand sign.
        (Integer(Char(_) | Short(_)), Integer(Char(_) | Short(_))) => Integer(Int(Signed)),
        // If both operands are `int`, but one is signed and the other is
        // unsigned, the result is unsigned.
        (Integer(Int(Signed)), Integer(Int(Unsigned))) | (Integer(Int(Unsigned)), Integer(Int(Signed))) => Integer(Int(Unsigned)),
        // No changes to homogenous operands of `int`s.
        (Integer(Int(Signed)), Integer(Int(Signed))) => Integer(Int(Signed)),
        (Integer(Int(Unsigned)), Integer(Int(Unsigned))) => Integer(Int(Unsigned)),
        // Operations involving long promote the other integer operand to
        // a `long` of the same sign as the long.
        (Integer(Long(sign)), Integer(Char(_) | Short(_) | Int(_))) => Integer(Long(*sign)),
        (Integer(Char(_) | Short(_) | Int(_)), Integer(Long(sign))) => Integer(Long(*sign)),
        // `long` follows the same rules as `int` for operands only
        // including `long` or `unsigned long`.
        (Integer(Long(Signed)), Integer(Long(Unsigned))) | (Integer(Long(Unsigned)), Integer(Long(Signed))) => Integer(Long(Unsigned)),
        (Integer(Long(Signed)), Integer(Long(Signed))) => Integer(Long(Signed)),
        (Integer(Long(Unsigned)), Integer(Long(Unsigned))) => Integer(Long(Unsigned)),
        // If one operand is any floating-point type and the other is an
        // integer type, the result will be a floating-point type with the
        // same precision as the floating-point operand.
        //(left @ (Float | Double), right) if right.is_integer() => left.clone(),
        //(left, right @ (Float | Double)) if left.is_integer() => right.clone(),
        // The only valid binary operator for structs is to assign from one
        // to the other, and they must be the same type.
        (left @ Struct(_), right @ Struct(_)) if left == right => left.clone(),
        _ => panic!("Invalid operation between types"), // TODO: return error instead
    }
}

/*
/// This function is based on a complex set of integer promotion rules
/// defined in Section 6.3.1 of the C11 standard.
pub fn arithmatic_determine_output(lhs: &CType, rhs: &CType) -> CType {
    use CType::*;
    use Sign::*;
    match (lhs, rhs) {
        // Binary operations on integers and pointers (which arrays can be implicitly
        // coerced into) always result in a pointer.
        (left @ (Ptr(_) | Array(_, _)), right) if right.is_integer() => left.clone(),
        (left, right @ (Ptr(_) | Array(_, _))) if left.is_integer() => right.clone(),
        // If one operand is `int` and the other is lower ranked than
        // `int`, the resulting type is `int` carrying the signed/unsigned
        // qualifier from the `int` operand.
        (Char(_) | Short(_), Int(sign)) => Int(*sign),
        (Int(sign), Char(_) | Short(_)) => Int(*sign),
        // If both operands are lower rank than `int` and `unsigned int`,
        // the result is always a signed `int`, regardless of operand sign.
        (Char(_) | Short(_), Char(_) | Short(_)) => Int(Signed),
        // If both operands are `int`, but one is signed and the other is
        // unsigned, the result is unsigned.
        (Int(Signed), Int(Unsigned)) | (Int(Unsigned), Int(Signed)) => Int(Unsigned),
        // No changes to homogenous operands of `int`s.
        (Int(Signed), Int(Signed)) => Int(Signed),
        (Int(Unsigned), Int(Unsigned)) => Int(Unsigned),
        // Operations involving long promote the other integer operand to
        // a `long` of the same sign as the long.
        (Long(sign), Char(_) | Short(_) | Int(_)) => Long(*sign),
        (Char(_) | Short(_) | Int(_), Long(sign)) => Long(*sign),
        // `long` follows the same rules as `int` for operands only
        // including `long` or `unsigned long`.
        (Long(Signed), Long(Unsigned)) | (Long(Unsigned), Long(Signed)) => Long(Unsigned),
        (Long(Signed), Long(Signed)) => Long(Signed),
        (Long(Unsigned), Long(Unsigned)) => Long(Unsigned),
        // If one operand is any floating-point type and the other is an
        // integer type, the result will be a floating-point type with the
        // same precision as the floating-point operand.
        (left @ (Float | Double), right) if right.is_integer() => left.clone(),
        (left, right @ (Float | Double)) if left.is_integer() => right.clone(),
        // The only valid binary operator for structs is to assign from one
        // to the other, and they must be the same type.
        (left @ Struct(_, _), right @ Struct(_, _)) if left == right => left.clone(),
        _ => panic!("Invalid operation between types"), // TODO: return error instead
    }
}
*/
