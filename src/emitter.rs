
use astgen::statement::Statement;
use astgen::expression::Expression;
use astgen::expression::operator::PreOp;

use crate::operand::Operand;


pub trait AssemblyEmitter {
    fn can_be_operand(&self) -> bool {
        false
    }

    fn as_operand(&self) -> Option<Operand> {
        None
    }

    fn operand_type(&self) -> Option<OperandType> {
        None
    }

    fn generate_instructions(&self) -> String;
}


// TODO: move this trait implementation elsewhere

impl AssemblyEmitter for Expression {
    fn can_be_operand(&self) -> bool {
        match self {
            Expression::IntegerLiteral(_) => true,
            Expression::Identifier(_) => false, // change back to true
            Expression::PrefixOperator(PreOp::Neg, box Expression::IntegerLiteral(value)) => true,
            _ => false,
        }
    }

    // TODO: make identifier -> operand work with non-static variables
    fn as_operand(&self) -> Option<Operand> {
        match self {
            Expression::IntegerLiteral(value) => Some(Operand::new(format!("${}", value))),
            Expression::Identifier(name) => {
                panic!("no");
                Some(Operand::new(format!("{}(%rip)", name)))
            }
            Expression::PrefixOperator(PreOp::Neg, box Expression::IntegerLiteral(value)) =>
                Some(Operand::new(format!("$-{}", value))),
            _ => None,
        }
    }

    fn operand_type(&self) -> Option<OperandType> {
        match self {
            Expression::IntegerLiteral(_) => Some(OperandType::Immediate),
            Expression::PrefixOperator(PreOp::Neg, box Expression::IntegerLiteral(value)) =>
                Some(OperandType::Immediate),
            Expression::Identifier(_) => Some(OperandType::Memory),
            _ => None,
        }
    }

    fn generate_instructions(&self) -> String {
        todo!()
    }
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum OperandType {
    Immediate,
    Register,
    Memory,
}
