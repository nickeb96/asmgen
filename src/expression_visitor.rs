
use std::collections::{HashMap, BTreeMap};

use maplit::{hashmap, btreemap};

use astgen::expression::Expression;
use astgen::expression::operator::*;
use astgen::ctype::CType;
use astgen::ctype::cinteger::{CInteger, Signedness};

use crate::register::Register;
use crate::location::{Location, DataWidth};
use crate::register_tracker::RegisterTracker;
use crate::emitter::{AssemblyEmitter, OperandType};
use crate::stack_manager::StackManager;
use crate::operand::Operand;
use crate::conversion;


pub struct ExpressionVisitor<'sm, 'rt> {
    stack: &'sm mut StackManager,
    registers: &'rt mut RegisterTracker,
    assembly: String,
}


impl<'sm, 'rt> ExpressionVisitor<'sm, 'rt> {
    pub fn new(stack: &'sm mut StackManager, registers: &'rt mut RegisterTracker) -> Self {
        Self {
            stack,
            registers,
            assembly: String::new(),
        }
    }

    fn append0(&mut self, instruction: &str) {
        self.assembly.push('\t');
        self.assembly.push_str(instruction);
        self.assembly.push('\n');
    }

    fn append1<T: Into<Operand>>(&mut self, instruction: &str, op: T) {
        let op: Operand = op.into();
        self.assembly.push('\t');
        self.assembly.push_str(instruction);
        self.assembly.push('\t');
        self.assembly.push_str(op.as_str());
        self.assembly.push('\n');
    }

    fn append2<T: Into<Operand>, U: Into<Operand>>(&mut self, instruction: &str, op1: T, op2: U) {
        let op1: Operand = op1.into();
        let op2: Operand = op2.into();
        self.assembly.push('\t');
        self.assembly.push_str(instruction);
        self.assembly.push('\t');
        self.assembly.push_str(op1.as_str());
        self.assembly.push_str(", ");
        self.assembly.push_str(op2.as_str());
        self.assembly.push('\n');
    }

    pub fn generate_assembly(&mut self, expression: &Expression, out_dest: Location) {
        //let result_ctype = expression.get_ctype();
        //let result_ctype = CType::Integer(CInteger::Int(Signedness::Signed));
        //let result_data_width = DataWidth::from_ctype(&result_ctype).unwrap();
        //let out_dest = Location::Register(result_data_width, out_register);
        match expression {
            Expression::IntegerLiteral(value) => {
                // TODO: add an IntLit type that supports .to_operand() and ensures values are within range
                let width = out_dest.get_data_width().unwrap();
                self.append2(&format!("mov{}", width.instruction_suffix()), Operand::new(format!("${}", value)), out_dest);
            }
            Expression::Identifier(name) => {
                if let Some(var) = self.stack.lookup(name) {
                    let var_location = Location::from(var);
                    let width = var_location.get_data_width().unwrap();
                    if !out_dest.is_memory() {
                        self.append2(&format!("mov{}", width.instruction_suffix()), var_location, out_dest);
                    } else {
                        let temp_register = self.registers.take_available().unwrap();
                        let temp_location = Location::Register(width, temp_register);
                        self.append2(&format!("mov{}", width.instruction_suffix()), var_location, temp_location);
                        self.append2(&format!("mov{}", width.instruction_suffix()), temp_location, out_dest);
                        self.registers.free_register(temp_register);
                    }
                } else {
                    panic!("tried to lookup {:?} in stack", name);
                    // lookup global variable
                    self.append2("mov", Operand::new("global_variable(%rip)"), out_dest);
                }
            }
            Expression::BinaryOperator(op, left_expression, right_expression) if op.associativity() == Associativity::LeftToRight => {
                let mut right_temp_register = None;
                // Try to use the right side as an operand if possible
                let right_operand: Operand = if right_expression.can_be_operand() {
                    right_expression.as_operand().unwrap()
                } else {
                    right_temp_register = Some(self.registers.take_available().unwrap());
                    //let right_ctype = right_expression.get_ctype();
                    let right_ctype = CType::Integer(CInteger::Int(Signedness::Signed));
                    let right_data_width = DataWidth::from_ctype(&right_ctype).unwrap();
                    let right_location = Location::Register(right_data_width, right_temp_register.unwrap());
                    self.generate_assembly(right_expression, right_location);
                    right_location.into()
                };

                match op {
                    BinOp::Add => {
                        self.generate_assembly(left_expression, out_dest);
                        self.append2("add", right_operand, out_dest);
                    }
                    BinOp::Sub => {
                        self.generate_assembly(left_expression, out_dest);
                        self.append2("sub", right_operand, out_dest);
                    }
                    BinOp::Mul => {
                        self.generate_assembly(left_expression, out_dest);
                        self.append2("imul", right_operand, out_dest);
                    }
                    BinOp::Div => {
                        let eax = Location::Register(DataWidth::DoubleWord, Register::A);
                        self.generate_assembly(left_expression, eax);
                        self.append0("cdq"); // sign extend eax into edx:eax
                        // divisor cannot be an immediate, use temporary
                        // register when necessary
                        if right_expression.operand_type() == Some(OperandType::Immediate) {
                            let temp_register = self.registers.take_available().unwrap();
                            let temp_location = Location::Register(DataWidth::DoubleWord, temp_register);
                            self.append2("mov", right_operand, temp_location);
                            self.append1("idivl", temp_location);
                            self.registers.free_register(temp_register);
                        } else {
                            self.append1("idivl", right_operand);
                        }
                        if out_dest != eax {
                            self.append2("mov", eax, out_dest);
                        }
                    }
                    BinOp::LessThan => {
                        let temp_register = self.registers.take_available().unwrap();
                        let temp_location = Location::Register(DataWidth::DoubleWord, temp_register);
                        self.generate_assembly(left_expression, temp_location);
                        self.append2("cmp", right_operand, temp_location);
                        self.append1("setl", out_dest);
                        self.registers.free_register(temp_register);
                    }
                    BinOp::GreaterThan => {
                        let temp_register = self.registers.take_available().unwrap();
                        let temp_location = Location::Register(DataWidth::DoubleWord, temp_register);
                        self.generate_assembly(left_expression, temp_location);
                        self.append2("cmp", right_operand, temp_location);
                        self.append1("setg", out_dest);
                        self.registers.free_register(temp_register);
                    }
                    BinOp::LessEqual => {
                        let temp_register = self.registers.take_available().unwrap();
                        let temp_location = Location::Register(DataWidth::DoubleWord, temp_register);
                        self.generate_assembly(left_expression, temp_location);
                        self.append2("cmp", right_operand, temp_location);
                        self.append1("setle", out_dest);
                        self.registers.free_register(temp_register);
                    }
                    BinOp::GreaterEqual => {
                        let temp_register = self.registers.take_available().unwrap();
                        let temp_location = Location::Register(DataWidth::DoubleWord, temp_register);
                        self.generate_assembly(left_expression, temp_location);
                        self.append2("cmp", right_operand, temp_location);
                        self.append1("setge", out_dest);
                        self.registers.free_register(temp_register);
                    }
                    _ => ()
                }

                // free temp register for right side if it was used
                if let Some(register) = right_temp_register {
                    self.registers.free_register(register);
                }
            }
            Expression::BinaryOperator(op, left_expression, right_expression) if op.associativity() == Associativity::RightToLeft => {
                let mut right_temp_register = None;
                // Try to use the right side as an operand if possible
                right_temp_register = Some(self.registers.take_available().unwrap());
                //let right_ctype = right_expression.get_ctype();
                let right_ctype = CType::Integer(CInteger::Int(Signedness::Signed));
                let right_data_width = DataWidth::from_ctype(&right_ctype).unwrap();
                let right_location = Location::Register(right_data_width, right_temp_register.unwrap());
                self.generate_assembly(right_expression, right_location);

                match op {
                    BinOp::Assign => {
                        // TODO: make this work when lhs is not an ident
                        if let box Expression::Identifier(ident) = left_expression {
                            let var = self.stack.lookup(ident).unwrap();
                            let left_location = Location::from(var);
                            let width = left_location.get_data_width().unwrap();
                            self.append2(&format!("mov{}", width.instruction_suffix()),
                                right_location, left_location);
                            if out_dest != Location::NoWhere {
                                self.append2(&format!("mov{}", width.instruction_suffix()),
                                right_location, out_dest);
                            }
                        } else {
                            panic!("can only assign to identifiers for now");
                        }
                    }
                    _ => todo!()
                }

                if let Some(register) = right_temp_register {
                    self.registers.free_register(register);
                }
            }
            Expression::PrefixOperator(op, child_expression) => {
                match op {
                    PreOp::Neg => {
                        self.generate_assembly(child_expression, out_dest);
                        self.append1("neg", out_dest);
                    }
                    PreOp::BitNot => {
                        self.generate_assembly(child_expression, out_dest);
                        self.append1("not", out_dest);
                    }
                    PreOp::Deref => {
                        let pointer_ctype = conversion::determine_output(child_expression, self.stack).unwrap();
                        let derefed_ctype = match pointer_ctype {
                            CType::Pointer(ref pointee) => pointee,
                            _ => panic!(),
                        };
                        let pointer_width = DataWidth::from_ctype(&pointer_ctype).unwrap(); // shoudl always be quadword
                        let temp_register = self.registers.take_available().unwrap();
                        let temp_location = Location::Register(pointer_width, temp_register);
                        self.generate_assembly(child_expression, temp_location);
                        self.assembly.push_str(&format!("\tmov{}\t({}), {}\n",
                                DataWidth::from_ctype(&derefed_ctype).unwrap().instruction_suffix(),
                                Operand::from(temp_location),
                                Operand::from(out_dest),
                        ));
                        self.registers.free_register(temp_register);
                    }
                    _ => ()
                }
            }
            Expression::PostfixOperator(PostOp::Subscript(index_expr), base_expr) => {
                let index_ctype = conversion::determine_output(index_expr, &self.stack).unwrap();
                let index_register = self.registers.take_available().unwrap();
                let index_location = Location::Register(DataWidth::from_ctype(&index_ctype).unwrap(), index_register);
                let base_ctype = conversion::determine_output(base_expr, &self.stack).unwrap();
                let base_register = self.registers.take_available().unwrap();
                let base_location = Location::Register(DataWidth::from_ctype(&base_ctype).unwrap(), base_register);
                let temp_register = self.registers.take_available().unwrap();
                let temp_location = Location::Register(out_dest.get_data_width().unwrap(), temp_register);
                self.generate_assembly(base_expr, base_location);
                // previous line puts the address of the pointer in the register, not the pointer itself
                //self.assembly.push_str(&format!("\tmovq\t({0}), {0}\n", Operand::from(base_location))); 
                self.generate_assembly(index_expr, index_location);
                self.assembly.push_str(
                    &format!("\tmov\t({}, {}, 4), {}\n",
                        Operand::from(base_location),
                        Operand::from(Location::Register(DataWidth::from_ctype(&base_ctype).unwrap(), index_register)),
                        Operand::from(temp_location),
                    )
                );
                self.append2("mov", temp_location, out_dest);
                self.registers.free_register(base_register);
                self.registers.free_register(index_register);
                self.registers.free_register(temp_register);
            }
            Expression::PostfixOperator(op, child_expression) => {
                todo!()
            }
            _ => unreachable!()
        }
    }

    pub fn get_assembly(&self) -> &str {
        &self.assembly
    }
}

