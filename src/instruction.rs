
use crate::operand::Operand;


#[derive(Debug)]
pub struct Instruction {
    content: String,
}

impl Instruction {
    pub fn new(content: String) -> Self {
        Self { content }
    }
}

impl From<Instruction> for String {
    fn from(instruction: Instruction) -> String {
        instruction.content
    }
}

impl std::fmt::Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.content)
    }
}


#[macro_export]
macro_rules! mnemonic {
    // internal
    (@__ $string:expr) => (
        crate::instruction::Instruction::new($string)
    );
    // no operands
    ($instruction_name:literal) => (
        mnemonic!(@__ format!("\t{}\n", $instruction_name))
    );
    // no operands with comment
    ($instruction_name:literal; $comment:expr) => (
        mnemonic!(@__ format!("\t{}  # {}\n", $instruction_name, $comment))
    );
    // one operand
    ($instruction_name:literal, $op1:expr) => (
        mnemonic!(@__ format!("\t{}\t{}\n", $instruction_name, Operand::from($op1)))
    );
    // one operand with comment
    ($instruction_name:literal, $op1:expr; $comment:expr) => (
        mnemonic!(@__ format!("\t{}\t{}  # {}\n", $instruction_name,
                Operand::from($op1), $comment))
    );
    // two operands
    ($instruction_name:literal, $op1:expr, $op2:expr) => (
        mnemonic!(@__ format!("\t{}\t{}, {}\n", $instruction_name,
                Operand::from($op1), Operand::from($op2)))
    );
    // two operands with comment
    ($instruction_name:literal, $op1:expr, $op2:expr; $comment:expr) => (
        mnemonic!(@__ format!("\t{}\t{}, {}  # {}\n", $instruction_name,
                Operand::from($op1), Operand::from($op2), $comment))
    );

}
