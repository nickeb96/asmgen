
use astgen::ctype::CType;
use astgen::ctype::cinteger::CInteger;
use astgen::identifier::Identifier;

use crate::register::Register;
use crate::operand::Operand;


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Location {
    Register(DataWidth, Register),
    StackOffset(DataWidth, usize),
    RipRelative(DataWidth, ()), // change () to identifier or something else
    FlagsRegister,
    NoWhere,
}

impl Location {
    pub fn get_data_width(&self) -> Option<DataWidth> {
        use Location::*;
        match self {
            Register(width, _) => Some(*width),
            StackOffset(width, _) => Some(*width),
            RipRelative(width, _) => Some(*width),
            FlagsRegister => None,
            NoWhere => None,
        }
    }

    pub fn is_memory(&self) -> bool {
        use Location::*;
        match self {
            Register(_, _) => false,
            StackOffset(_, _) => true,
            RipRelative(_, _) => true,
            FlagsRegister => false,
            NoWhere => false,
        }
    }
}

impl From<Location> for Operand {
    fn from(loc: Location) -> Operand {
        use Location::*;
        Operand::new(match loc {
            Register(size, reg) => format!("%{}", reg.sized_name(size)),
            StackOffset(size, offset) => format!("-{}(%rbp)", offset),
            RipRelative(size, _) => {
                panic!("dont do this yet");
                format!("{}(%rip)", "my_global_var")
            }
            FlagsRegister => String::new(),
            NoWhere => String::new(),
        })
    }
}


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum DataWidth {
    Byte = 0,
    Word = 1,
    DoubleWord = 2,
    QuadWord = 3,
}

impl DataWidth {
    pub fn instruction_suffix(&self) -> &'static str {
        use DataWidth::*;
        match self {
            Byte => "b",
            Word => "w",
            DoubleWord => "l",
            QuadWord => "q",
        }
    }

    pub fn from_bit_count(bits: usize) -> Result<Self, ()> {
        use DataWidth::*;
        match bits {
            8 => Ok(Byte),
            16 => Ok(Word),
            32 => Ok(DoubleWord),
            64 => Ok(QuadWord),
            _ => Err(()),
        }
    }

    pub fn from_byte_count(bytes: usize) -> Result<Self, ()> {
        Self::from_bit_count(bytes * 8)
    }

    pub fn from_ctype(ctype: &CType) -> Result<Self, ()> {
        use DataWidth::*;
        match ctype {
            CType::Integer(CInteger::Char(_)) => Ok(Byte),
            CType::Integer(CInteger::Short(_)) => Ok(Word),
            CType::Integer(CInteger::Int(_)) => Ok(DoubleWord),
            CType::Integer(CInteger::Long(_)) => Ok(QuadWord),
            CType::Pointer(_) => Ok(QuadWord),
            _ => panic!("Can't do these yet")
        }
    }
}


