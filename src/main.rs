#![feature(box_syntax, box_patterns, bindings_after_at, or_patterns)]

#![allow(unused)]

use std::io::prelude::*;

mod operand;
mod emitter;
mod location;
mod register;
mod memory_layout;
mod register_tracker;
mod symbol_table;
//mod variable_table;
mod expression_visitor;
mod procedure;
mod stack_manager;
mod statement_visitor;
mod instruction;
mod conversion;

use astgen::expression;
use astgen::ctype::CType;
use astgen::ctype::cinteger::{CInteger, Signedness};
use astgen::definition::Definition;
use astgen::expression::Expression;
use astgen::expression::operator::*;
use astgen::expression::parser;
use astgen::identifier::Identifier;

use register::*;
use location::*;

fn badmain() {
    for i in 0..32 {
        println!("{}: {}", i, stack_manager::determine_padding(i, 4));
    }
}

fn main() {
    let mut args = std::env::args().skip(1);
    let source = args.next().expect("Missing input file");
    let destination = args.next().expect("Missing output file");

    let input = std::fs::read_to_string(&source).expect("Could not read input file");
    let mut output_file = std::fs::File::create(destination).expect("Could not create output file");

    let mut definitions = astgen::file_parser::parse_file(&input).unwrap();
    let mut symbols = Vec::new();
    let mut label_counter = 100;

    output_file.write(".text\n".as_bytes());
    for def in definitions {
        match &def {
            function @ Definition::Function(symbol, _, _) => {
                symbols.push(format!("_{}", symbol.as_str()));
                let mut procedure = procedure::ProcedureCreator::new(label_counter);
                label_counter = procedure.get_label_counter();
                procedure.assembly_for_function(function);
                let assembly = procedure.get_assembly();
                output_file.write(assembly.as_bytes());
            }
            _ => panic!("can't do that yet")
        }
    }

    output_file.write(format!(".global {}\n", symbols.join(", ")).as_bytes());
}


/*
fn presentation_main() {
    let s = r"
    int factorial(int n) {
        int result = 1;
        while (n >= 2) {
            result = result * n;
            n = n - 1;
        }
        return result;
    }
    ";

    let mut definitions = astgen::file_parser::parse_file(s).unwrap();

    match definitions[0] {
        Definition::Function(_, _, _) => {
            let mut procedure = procedure::ProcedureCreator::new();
            procedure.assembly_for_function(&definitions[0]);
            let assembly = procedure.get_assembly();
            println!("{}", assembly);
        }
        _ => panic!()
    }
}
*/


fn stack_test_main() -> Result<(), Box<dyn std::error::Error>> {
    let mut instructions = Vec::<String>::new();
    let mut sm = stack_manager::StackManager::new();
    sm.declare_var(Identifier::new("x")?, CType::Integer(CInteger::Int(Signedness::Signed)));
    sm.declare_var(Identifier::new("ptr")?, CType::Pointer(box CType::Void));
    sm.declare_var(Identifier::new("c")?, CType::Integer(CInteger::Char(Signedness::Signed)));
    println!("{:#?}", sm);

    Ok(())
}

/*
fn expression_visitor_main() {
    let s = "4 * (2 + 9) + -1";
    let tokens: Vec<_> = tokenizer::iter_tokens(s).collect();
    let root = parser::fragment_list_into_expression(tokens);

    let mut visitor = expression_visitor::ExpressionVisitor::new();

    visitor.generate_assembly(
        &root,
        Register::B,
    );

    eprintln!("og expression: {}\n", root);
    print!("{}", visitor.get_assembly());
}
*/
