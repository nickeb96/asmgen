
use astgen::ctype::CType;
use astgen::ctype::cinteger::{CInteger, Signedness};

use crate::location::DataWidth;


#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct IncompleteType(CType);

impl std::fmt::Display for IncompleteType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Incomplete type: {}", self.0)
    }
}


pub trait MemoryLayout {
    fn size(&self) -> Result<usize, IncompleteType>;
    fn alignment(&self) -> Result<usize, IncompleteType>;
}


impl MemoryLayout for CType {
    /// Size of a given type
    fn size(&self) -> Result<usize, IncompleteType> {
        use CType::*;
        use CInteger::*;
        Ok(match self {
            Void => 1,
            Integer(Char(_)) => 1,
            Integer(Short(_)) => 2,
            Integer(Int(_)) => 4,
            Integer(Long(_)) => 8,
            Pointer(_) => 8,
            Array(element_type, Some(length)) => element_type.size()? * length,
            Array(element_type, None) => return Err(IncompleteType(self.clone())),
            Struct(cstruct) => {
                let mut struct_size = 0;
                let mut field_iter = cstruct.fields().iter();
                // first field is never aligned
                if let Some((field_name, field_type)) = field_iter.next() {
                    struct_size += field_type.size()?;
                }
                while let Some((field_name, field_type)) = field_iter.next() {
                    let align = field_type.alignment()?;
                    // pad before field to reach alignment
                    let padding = align - 1 - (struct_size - 1) % align;
                    struct_size += padding + field_type.size()?;
                }
                // pad end of struct when necessary
                let highest_align = cstruct.fields().iter().map(
                    |(_, ctype)| ctype.alignment().unwrap()).max().unwrap();
                let tail_padding = highest_align - 1 - (struct_size - 1) % highest_align;
                struct_size + tail_padding
            }
        })
    }

    /// Alignment requirements of a given type
    ///
    /// Most primitive types have the same alignment as their size.  Composite
    /// types have the same alignment requirement as their subtype with the
    /// highest alignment requirement.
    ///
    /// Examples:
    /// 
    /// ```c
    /// int x; // alignment is 4
    /// short y: // alignment is 2
    /// int* ptr; // alignment is 8
    ///
    /// struct person {
    ///     short age; // bytes 0..2
    ///     // [2 bytes of padding]
    ///     int ssn; // bytes 4..8
    ///     short favorite_numbers[3]; // bytes 8..14
    ///     char middle_initial; // byte 14..15
    ///     // [1 byte of padding]
    ///     char* name; // bytes 16..24
    ///     float height; // bytes 24..28
    ///     // [4 bytes of tail padding]
    /// };
    /// 
    /// // total size of person is 32 bytes
    /// // alignment requirement for person is 8 bytes (because of char*)
    /// ```
    fn alignment(&self) -> Result<usize, IncompleteType> {
        use CType::*;
        match self {
            Struct(cstruct) => {
                Ok(cstruct.fields().iter().map(
                        |(_, ctype)| ctype.alignment().unwrap()).max().unwrap())
            }
            Array(element_type, _) => element_type.alignment(),
            other => other.size(),
        }
    }
}
