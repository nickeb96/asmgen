
use std::collections::{HashMap, BTreeMap};

use maplit::{hashmap, btreemap};

use astgen::expression::Expression;
use astgen::expression::operator::*;
use astgen::ctype::CType;
use astgen::ctype::cinteger::{CInteger, Signedness};

use crate::register::Register;
use crate::location::{Location, DataWidth};
use crate::register_tracker::RegisterTracker;
use crate::emitter::AssemblyEmitter;
use crate::operand::Operand;



pub struct ExpressionVisitor {
    registers: RegisterTracker,
    assembly: String,
}

impl ExpressionVisitor {
    pub fn new() -> Self {
        Self {
            registers: RegisterTracker::new(),
            assembly: String::new(),
        }
    }

    pub fn get_assembly(&self) -> &str {
        &self.assembly
    }

    fn append1<T: Into<Operand>>(&mut self, instruction: &str, op: T) {
        let op: Operand = op.into();
        self.assembly.push('\t');
        self.assembly.push_str(instruction);
        self.assembly.push('\t');
        self.assembly.push_str(op.as_str());
        self.assembly.push('\n');
    }

    fn append2<T: Into<Operand>, U: Into<Operand>>(&mut self, instruction: &str, op1: T, op2: U) {
        let op1: Operand = op1.into();
        let op2: Operand = op2.into();
        self.assembly.push('\t');
        self.assembly.push_str(instruction);
        self.assembly.push('\t');
        self.assembly.push_str(op1.as_str());
        self.assembly.push_str(", ");
        self.assembly.push_str(op2.as_str());
        self.assembly.push('\n');
    }

    pub fn generate_assembly(&mut self, expression: &Expression, out_register: Register) {
        //let result_ctype = expression.get_ctype();
        let result_ctype = CType::Integer(CInteger::Int(Signedness::Signed));
        let result_data_width = DataWidth::from_ctype(&result_ctype).unwrap();
        let out_dest = Location::Register(result_data_width, out_register);
        match expression {
            Expression::IntegerLiteral(value) => {
                // TODO: add an IntLit type that supports .to_operand() and ensures values are within range
                self.append2("mov", Operand::new(format!("${}", value)), out_dest);
            }
            Expression::Identifier(name) => {
                //self.append2("mov", );
                // this only works with static variables
                /*self.append(&format!(
                        "mov {}(%rip), {}\n",
                        name,
                        destination.to_operand()));*/
            }
            Expression::BinaryOperator(op, left_expression, right_expression) if op.associativity() == Associativity::LeftToRight => {
                self.generate_assembly(left_expression, out_register);

                let mut right_temp_register = None;
                // Try to use the right side as an operand if possible
                let right_operand: Operand = if right_expression.can_be_operand() {
                    right_expression.as_operand().unwrap()
                } else {
                    right_temp_register = Some(self.registers.take_available().unwrap());
                    self.generate_assembly(right_expression, right_temp_register.unwrap());
                    //let right_ctype = right_expression.get_ctype();
                    let right_ctype = CType::Integer(CInteger::Int(Signedness::Signed));
                    let right_data_width = DataWidth::from_ctype(&right_ctype).unwrap();
                    Location::Register(right_data_width, right_temp_register.unwrap()).into()
                };

                match op {
                    BinOp::Add => {
                        self.append2("add", right_operand, out_dest);
                    }
                    BinOp::Sub => {
                        self.append2("sub", right_operand, out_dest);
                    }
                    BinOp::Mul => {
                        self.append2("imul", right_operand, out_dest);
                    }
                    BinOp::LessThan => {
                        self.append2("cmp", right_operand, out_dest);
                    }
                    _ => ()
                }

                // free temp register for right side if it was used
                if let Some(register) = right_temp_register {
                    self.registers.free_register(register);
                }
            }
            Expression::BinaryOperator(op, left_expression, right_expression) if op.associativity() == Associativity::RightToLeft => {
                todo!()
            }
            Expression::PrefixOperator(op, child_expression) => {
                self.generate_assembly(child_expression, out_register);
                match op {
                    PreOp::Neg => {
                        self.append1("neg", out_dest);
                    }
                    PreOp::BitNot => {
                        self.append1("not", out_dest);
                    }
                    _ => ()
                }
            }
            Expression::PostfixOperator(op, child_expression) => {
                todo!()
            }
            _ => unreachable!()
        }
    }
}
