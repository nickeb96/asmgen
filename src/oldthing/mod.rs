
mod leaves;


pub trait Emitter {
    /// Rvalue will put the value of an expression at the destination
    pub fn emit_rvalue(&self, destination: Location) -> String;

    /// Lvalues will put the address of an lvalue a the destination, if it is
    /// possible
    pub fn emit_lvalue(&self, destination: Location) -> Option<String> {
        None
    }
}

