
use astgen::definition::Definition;
use astgen::statement::Statement;

use crate::stack_manager::{StackManager, StackVariable};
use crate::register_tracker::RegisterTracker;
use crate::register::Register;
use crate::location::{Location, DataWidth};
use crate::operand::Operand;
use crate::statement_visitor::StatementVisitor;
use crate::mnemonic;


/// This is the list of registers that are used for passing function arguments
/// to a function.  Arguments past 6 are passed on the stack.
pub const REGISTER_ARG_ORDER: [Register; 6] = [
    Register::DI,
    Register::SI,
    Register::D,
    Register::C,
    Register::R8,
    Register::R9,
    // remaining go on stack
];


pub struct ProcedureCreator {
    pub stack: StackManager,
    registers: RegisterTracker,
    label_counter: usize,
    assembly: String,
}

impl ProcedureCreator {
    pub fn new(label_counter_start: usize) -> Self {
        Self {
            stack: StackManager::new(),
            registers: RegisterTracker::new(),
            label_counter: label_counter_start,
            assembly: String::new(),
        }
    }

    pub fn assembly_for_function(&mut self, function_definition: &Definition) {
        let (function_name, cfunction, body) = match function_definition {
            Definition::Function(a, b, c) => (a, b, c),
            _ => panic!("invalid arg"),
        };

        self.assembly.push_str(&format!("_{}:\n", function_name));
        self.assembly.push_str(&format!("\tpushq\t%rbp\n"));
        self.assembly.push_str(&format!("\tmovq\t%rsp, %rbp\n"));

        let mut arg_move_instructions: Vec<String> = Vec::new();
        for (arg_number, (maybe_identifier, ctype)) in cfunction.iter_args().enumerate() {
            // ignore functions with unnamed args
            if let Some(identifier) = maybe_identifier {
                // handle args passed in through registers
                if let Some(arg_register) = REGISTER_ARG_ORDER.get(arg_number) {
                    self.stack.declare_var(identifier.clone(), ctype.clone());
                    // lookup the stack_offset for the newly added variable
                    let StackVariable {
                        ctype: _,
                        stack_offset,
                    } = self.stack.lookup(&identifier).unwrap();
                    let src = Location::Register(DataWidth::from_ctype(ctype).unwrap(), *arg_register);
                    let dst = Location::StackOffset(DataWidth::from_ctype(ctype).unwrap(), *stack_offset);
                    arg_move_instructions.push(
                        mnemonic!("mov", src, dst; format!("setup arg '{}'", identifier)).to_string());
                } else {
                    panic!("unable to have functions with more than 6 args yet");
                }
            }
        }
        // collect all stack offsets from parameter instantiation into one
        // stack offset instruction
        if let Some(stack_changes) = self.stack.reconcile_stack() {
            self.assembly.push_str(&stack_changes);
        }
        // place arg values from registers into stack locations
        for arg in arg_move_instructions {
            self.assembly.push_str(&arg);
        }

        let mut statement_visitor = StatementVisitor::new(
            &mut self.stack, &mut self.registers, &mut self.label_counter);

        // walk function body
        for statement in body.iter_statements() {
            statement_visitor.generate_assembly(statement);
        }
        self.assembly.push_str(&statement_visitor.get_assembly());
        // if the function body does not end with a return statement, insert an
        // implicit one
        if !matches!(body.iter_statements().last(), Some(Statement::Return(_))) {
            self.assembly.push_str("\tmov\t%rbp, %rsp\n");
            self.assembly.push_str("\tpop\t%rbp\n");
            self.assembly.push_str("\tret\n");
            
        }
    }

    pub fn get_assembly(&self) -> &str {
        &self.assembly
    }

    pub fn get_label_counter(&self) -> usize {
        self.label_counter
    }
}

