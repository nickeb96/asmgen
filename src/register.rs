
use crate::location::DataWidth;


const NAMED_REGISTER_MAP: [[&'static str; 4]; 8] = [
    ["al", "ax", "eax", "rax"],
    ["bl", "bx", "ebx", "rbx"],
    ["cl", "dx", "ecx", "rcx"],
    ["dl", "dx", "edx", "rdx"],
    ["spl", "sp", "esp", "rsp"],
    ["bpl", "bp", "ebp", "rbp"],
    ["sil", "si", "esi", "rsi"],
    ["dil", "di", "edi", "rsi"],
];


#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Register {
    A = 0,
    B = 1,
    C = 2,
    D = 3,
    SP = 4,
    BP = 5,
    SI = 6,
    DI = 7,
    R8 = 8,
    R9 = 9,
    R10 = 10,
    R11 = 11,
    R12 = 12,
    R13 = 13,
    R14 = 14,
    R15 = 15,
}

impl Register {
    pub fn sized_name(self, register_width: DataWidth) -> String {
        match self as usize {
            named_register @ 0..=7 => {
                NAMED_REGISTER_MAP[named_register][register_width as usize].to_owned()
            }
            numbered_register => match register_width {
                DataWidth::Byte => format!("r{}b", numbered_register),
                DataWidth::Word => format!("r{}w", numbered_register),
                DataWidth::DoubleWord => format!("r{}d", numbered_register),
                DataWidth::QuadWord => format!("r{}", numbered_register),
            }
        }
    }
}

