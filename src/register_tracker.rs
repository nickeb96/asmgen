use std::collections::BTreeMap;
use std::cell::RefCell;
use std::rc::{Rc, Weak};

use maplit::btreemap;

use crate::register::Register;


pub struct BorrowedRegister {
    register: Register,
    available: Weak<RefCell<BTreeMap<Register, bool>>>,
}

impl Drop for BorrowedRegister {
    fn drop(&mut self) {
        if let Some(available) = Weak::upgrade(&self.available) {
            *available.borrow_mut().get_mut(&self.register).unwrap() = true;
        }
    }
}

impl std::ops::Deref for BorrowedRegister {
    type Target = Register;
    fn deref(&self) -> &Self::Target {
        &self.register
    }
}

pub struct RegisterTracker {
    available: Rc<RefCell<BTreeMap<Register, bool>>>,
}

impl RegisterTracker {
    pub fn new() -> Self {
        use Register::*;
        Self {
            available: Rc::new(RefCell::new(btreemap!{
                //A => true,
                C => true,
                //D => true, // can't really use D because it's used in multiplication and division
                DI => true,
                SI => true,
                R8 => true,
                R9 => true,
                R10 => true,
                R11 => true,
            }))
        }
    }

    pub fn take_available(&mut self) -> Option<Register> {
        for (register, free) in self.available.borrow_mut().iter_mut() {
            if *free {
                *free = false;
                return Some(*register);
            }
        }
        None
    }

    pub fn auto_take_available(&mut self) -> Option<BorrowedRegister> {
        for (register, free) in self.available.borrow_mut().iter_mut() {
            if *free {
                *free = false;
                return Some(BorrowedRegister {
                    register: *register,
                    available: Rc::downgrade(&self.available),
                });
            }
        }
        None
    }

    pub fn free_register(&mut self, register: Register) {
        *self.available.borrow_mut().get_mut(&register).unwrap() = true;
    }
}
