
use std::collections::HashMap;
use std::cmp::Reverse;

use astgen::ctype::CType;
use astgen::identifier::Identifier;

use crate::memory_layout::MemoryLayout;
use crate::location::{Location, DataWidth};


const STACK_ALIGNMENT: usize = 16;


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct StackVariable {
    pub ctype: CType,
    pub stack_offset: usize,
}

impl StackVariable {
    pub fn new(ctype: CType, stack_offset: usize) -> Self {
        Self {
            ctype,
            stack_offset,
        }
    }
}

impl From<&StackVariable> for Location {
    fn from(var: &StackVariable) -> Location {
        let width = DataWidth::from_ctype(&var.ctype).unwrap();
        Location::StackOffset(width, var.stack_offset)
    }
}


pub fn determine_padding(current: usize, align: usize) -> usize {
    if current > 0 {
        align - 1 - (current - 1) % align
    } else {
        0
    }
}


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct StackManager {
    variable_map: HashMap<Identifier, StackVariable>,
    // the "virtual" top of stack, value not aligned to a STACK_ALIGNMENT boundary
    top_of_stack: usize,
    // remaining space until the next STACK_ALIGNMENT boundary
    additional_capacity: usize,
    // the actual value of rsp
    real_top_of_stack: usize,
}

impl StackManager {
    pub fn new() -> Self {
        Self {
            variable_map: HashMap::new(),
            top_of_stack: 0,
            additional_capacity: 0,
            real_top_of_stack: 0,
        }
    }

    pub fn stack_space(&self) -> usize {
        self.top_of_stack + self.additional_capacity
    }

    pub fn lookup(&self, identifier: &Identifier) -> Option<&StackVariable> {
        self.variable_map.get(identifier)
    }

    pub fn declare_var(&mut self, identifier: Identifier, ctype: CType) {
        let size = ctype.size().unwrap();
        let align = ctype.alignment().unwrap();
        // padding *after* variable on stack to satisfy alignment requirement
        // of ctype
        let padding = determine_padding(self.top_of_stack, align);
        // %rbp relative "address" of variable
        let variable_offset = self.top_of_stack + padding + size;
        let additional_space_needed = padding + size;
        // if more space needs to be allocated on stack `allocate_space(...)`
        self.allocate_space(additional_space_needed);
        self.variable_map.insert(identifier, StackVariable::new(ctype, variable_offset));
    }

    pub fn allocate_space(&mut self, bytes_needed: usize) {
        if bytes_needed <= self.additional_capacity {
            self.additional_capacity -= bytes_needed;
            self.top_of_stack += bytes_needed;
        } else {
            let aligned_top_of_stack = self.top_of_stack + self.additional_capacity;
            assert!(aligned_top_of_stack % STACK_ALIGNMENT == 0);
            let real_bytes_needed = bytes_needed - self.additional_capacity;
            assert!(real_bytes_needed > 0);
            let tail_padding = determine_padding(real_bytes_needed, STACK_ALIGNMENT);
            let bytes_to_allocate = real_bytes_needed + tail_padding;
            self.top_of_stack += real_bytes_needed;
            self.additional_capacity = tail_padding;
        }
    }

    /// Create the instructions needed to reconcile %rsp
    pub fn reconcile_stack(&mut self) -> Option<String> {
        let target_top_of_stack = self.top_of_stack + self.additional_capacity;
        assert_eq!(target_top_of_stack % STACK_ALIGNMENT, 0);
        let diff = target_top_of_stack as isize - self.real_top_of_stack as isize;
        assert_eq!(diff.abs() as usize % STACK_ALIGNMENT, 0);
        // update expected value of rsp
        self.real_top_of_stack = ((self.real_top_of_stack as isize) + diff) as usize;
        if diff > 0 {
            // stack must grow
            Some(format!("\tsubq\t${}, %rsp\n", diff))
        } else if diff < 0 {
            // stack must shrink
            Some(format!("\taddq\t${}, %rsp\n", diff.abs()))
        } else {
            // no changes to stack pointer
            None
        }
    }

    pub fn destroy_vars<I: IntoIterator<Item=Identifier>>(&mut self, var_iter: I) -> Option<String> {
        let mut vars: Vec<_> = var_iter.into_iter().map(
            |ident| (&self.variable_map[&ident], ident)).collect();
        vars.sort_by_key(|(var, ident)| Reverse(var.stack_offset));
        println!("to delete: {:?}", vars);
        None
    }
}

