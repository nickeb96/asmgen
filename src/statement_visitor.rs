
use astgen::statement::Statement;
use astgen::expression::Expression;

use crate::expression_visitor::ExpressionVisitor;
use crate::register::Register;
use crate::location::{Location, DataWidth};
use crate::operand::Operand;
use crate::register_tracker::RegisterTracker;
use crate::stack_manager::StackManager;
use crate::conversion;


pub struct StatementVisitor<'sm, 'rt, 'lc> {
    stack: &'sm mut StackManager,
    registers: &'rt mut RegisterTracker,
    label_counter: &'lc mut usize,
    assembly: String,
}

impl<'sm, 'rt, 'lc> StatementVisitor<'sm, 'rt, 'lc> {
    pub fn new(
        stack: &'sm mut StackManager,
        registers: &'rt mut RegisterTracker,
        label_counter: &'lc mut usize,
    ) -> Self {
        Self {
            stack,
            registers,
            label_counter,
            assembly: String::new(),
        }
    }

    pub fn get_assembly(&self) -> &str {
        &self.assembly
    }

    pub fn generate_assembly(&mut self, statement: &Statement) {
        //println!("DOING: {}\nstack: {:#?}", statement, self.stack);
        match statement {
            Statement::VariableDeclaration(ident, ctype, maybe_init) => {
                self.stack.declare_var(ident.clone(), ctype.clone());
                if let Some(instruction) = self.stack.reconcile_stack() {
                    self.assembly.push_str(&instruction);
                }
                if let Some(init) = maybe_init {
                    let stack_var = self.stack.lookup(ident).unwrap();
                    let width = DataWidth::from_ctype(ctype).unwrap();
                    let location = Location::StackOffset(width, stack_var.stack_offset);
                    let mut expression_visitor = ExpressionVisitor::new(self.stack, self.registers);
                    expression_visitor.generate_assembly(init, location);
                    self.assembly.push_str(expression_visitor.get_assembly());
                }
            }
            Statement::Single(expression) => {
                let mut expression_visitor = ExpressionVisitor::new(self.stack, self.registers);
                expression_visitor.generate_assembly(expression, Location::NoWhere);
                let instructions = expression_visitor.get_assembly();
                self.assembly.push_str(instructions);
            }
            Statement::Block(block) => {
                for statement in block.iter_statements() {
                    self.generate_assembly(statement);
                }
            }
            Statement::WhileLoop(condition, body) => {
                let begin_label = self.generate_label();
                let end_label = self.generate_label();
                self.assembly.push_str(&format!("{}:\n", begin_label));
                let condition_dst = Location::Register(DataWidth::Byte, Register::A);
                let condition_operand = Operand::from(condition_dst);
                let mut expr_visitor = ExpressionVisitor::new(self.stack, self.registers);
                expr_visitor.generate_assembly(condition, Location::Register(DataWidth::Byte, Register::A));
                let condition_instructions = expr_visitor.get_assembly();
                self.assembly.push_str(condition_instructions);
                self.assembly.push_str(&format!("\ttest\t{0}, {0}\n", condition_operand.as_str()));
                self.assembly.push_str(&format!("\tjz\t{}\n", end_label));
                self.generate_assembly(body);
                self.assembly.push_str(&format!("\tjmp\t{}\n", begin_label));
                self.assembly.push_str(&format!("{}:\n", end_label));
            }
            Statement::IfElse(condition, true_body, maybe_false_body) => {
                let if_end_label = self.generate_label();
                let maybe_else_end_label = maybe_false_body.as_ref().map(|_| self.generate_label());
                let condition_dst = Location::Register(DataWidth::Byte, Register::A);
                let condition_operand = Operand::from(condition_dst);
                let mut expr_visitor = ExpressionVisitor::new(self.stack, self.registers);
                expr_visitor.generate_assembly(condition, Location::Register(DataWidth::Byte, Register::A));
                let condition_instructions = expr_visitor.get_assembly();
                self.assembly.push_str(condition_instructions);
                self.assembly.push_str(&format!("\ttest\t{0}, {0}\n", condition_operand.as_str()));
                self.assembly.push_str(&format!("\tjz\t{}\n", if_end_label));
                self.generate_assembly(true_body);
                if let Some(else_end_label) = maybe_else_end_label.as_ref() {
                    self.assembly.push_str(&format!("\tjmp\t{}\n", else_end_label));
                }
                self.assembly.push_str(&format!("{}:\n", if_end_label));
                if let Some(false_body) = maybe_false_body {
                    self.generate_assembly(false_body);
                    self.assembly.push_str(&format!("{}:\n", maybe_else_end_label.unwrap()));
                }
            }
            Statement::Return(Some(expression)) => {
                let expression_ctype = conversion::determine_output(expression, self.stack).unwrap();
                let mut expression_visitor = ExpressionVisitor::new(self.stack, self.registers);
                expression_visitor.generate_assembly(expression, Location::Register(DataWidth::from_ctype(&expression_ctype).unwrap(), Register::A));
                let instructions = expression_visitor.get_assembly();
                self.assembly.push_str(instructions);
                self.assembly.push_str("\tmovq\t%rbp, %rsp\n");
                self.assembly.push_str("\tpopq\t%rbp\n");
                self.assembly.push_str("\tret\n");
            }
            Statement::Return(None) => {
                self.assembly.push_str("\tmovq\t%rbp, %rsp\n");
                self.assembly.push_str("\tpopq\t%rbp\n");
                self.assembly.push_str("\tret\n");
            }
            other => panic!("can't do {:?} yet", other)
        }
    }

    fn generate_label(&mut self) -> String {
        *self.label_counter += 1;
        let ret = format!("L{:03}", self.label_counter);
        ret
    }
}
