
use std::collections::HashMap;

use astgen::ctype::CType;


#[derive(Debug, Clone, Eq, PartialEq)]
pub enum GlobalSymbol {
    StaticVariable {
        ctype: CType,
    },
    Function {
        return_ctype: CType,
        arguments: Vec<(String, CType)>,
    },
}


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct SymbolTable {
    symbols: HashMap<String, GlobalSymbol>,
}

