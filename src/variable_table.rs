
use std::collections::HashMap;

use astgen::ctype::CType;

use crate::memory_layout::MemoryLayout;

const STACK_ALIGNMENT: usize = 16;


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct StackVariable {
    ctype: CType,
    stack_offset: usize,
}

impl StackVariable {
    pub fn new(ctype: CType, stack_offset: usize) -> Self {
        Self {
            ctype,
            stack_offset,
        }
    }
}


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct VariableTable {
    variables: Vec<StackVariable>,
    name_map: HashMap<String, usize>,
    top_of_stack: usize, // bytes used on stack; not stack aligned
    capacity: usize, // additional capacity available above `top_of_stack`, up to alignment requirement
}

impl VariableTable {
    pub fn new() -> Self {
        Self {
            variables: Vec::new(),
            name_map: HashMap::new(),
            top_of_stack: 0,
            capacity: 0,
        }
    }

    pub fn stack_space(&self) -> usize {
        self.top_of_stack + self.capacity
    }

    pub fn lookup(&self, name: &str) -> Option<StackVariable> {
        let index = self.name_map.get(name)?;
        Some(self.variables[*index].clone())
    }

    pub fn declare_var(&mut self, name: String, ctype: CType) -> Option<String> {
        let size = ctype.size().unwrap();
        let align = ctype.alignment().unwrap();
        // padding *before* variable on stack to satisfy alignment requirement
        // of ctype
        let padding = self.top_of_stack - 1 - (self.top_of_stack - 1) % align;
        // rbp relative "address" of variable
        let variable_offset = self.top_of_stack + padding;
        let additional_space_needed = padding + size;
        // if more space needs to be allocated on, stack `allocate_space(...)`
        // will return the instructions needed to do so
        let instructions = self.allocate_space(additional_space_needed);
        self.name_map.insert(name, self.variables.len());
        self.variables.push(StackVariable::new(ctype, variable_offset));
        instructions
    }

    pub fn allocate_space(&mut self, bytes_needed: usize) -> Option<String> {
        if bytes_needed < self.capacity {
            self.capacity -= bytes_needed;
            None
        } else {
            let real_top_of_stack = self.top_of_stack + self.capacity;
            assert!(real_top_of_stack % STACK_ALIGNMENT == 0);
            let real_bytes_needed = bytes_needed - self.capacity;
            let tail_padding = STACK_ALIGNMENT - 1 - (real_bytes_needed - 1) % STACK_ALIGNMENT;
            let bytes_to_allocate = real_bytes_needed + tail_padding;
            self.top_of_stack = real_bytes_needed;
            self.capacity = tail_padding;
            Some(format!("\tsubq\t${}, %rsp", bytes_to_allocate))
        }
    }

    pub fn destroy_vars<I: IntoIterator<Item=StackVariable>>(&mut self, vars: I) -> Option<String> {
        for var in vars {
        }
        None
    }
}

